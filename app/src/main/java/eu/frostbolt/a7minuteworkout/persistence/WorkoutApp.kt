package eu.frostbolt.a7minuteworkout.persistence

import android.app.Application

class WorkoutApp : Application() {
    val db by lazy { HistoryDatabase.getInstance(this) }
}