package eu.frostbolt.a7minuteworkout.persistence

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "history")
data class HistoryEntity(
    @PrimaryKey
    val date: String
)
