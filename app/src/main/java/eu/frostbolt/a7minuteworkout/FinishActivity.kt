package eu.frostbolt.a7minuteworkout

import android.icu.text.SimpleDateFormat
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.lifecycleScope
import eu.frostbolt.a7minuteworkout.databinding.ActivityFinishBinding
import eu.frostbolt.a7minuteworkout.persistence.HistoryDao
import eu.frostbolt.a7minuteworkout.persistence.HistoryEntity
import eu.frostbolt.a7minuteworkout.persistence.WorkoutApp
import kotlinx.coroutines.launch
import java.util.*

class FinishActivity : AppCompatActivity() {
    private var binding: ActivityFinishBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityFinishBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        setSupportActionBar(binding?.tbFinish)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding?.tbFinish?.setNavigationOnClickListener { onBackPressed() }

        binding?.btnFinish?.setOnClickListener { finish() }

        val historyDao = (application as WorkoutApp).db.historyDao()
        createDate(historyDao)
    }

    private fun createDate(historyDao: HistoryDao) {
        val calendarInstance = Calendar.getInstance()
        val simpleDateFormat = SimpleDateFormat("dd MMM yyyy HH:mm:ss", Locale.getDefault())

        val dateTime = calendarInstance.time
        val date = simpleDateFormat.format(dateTime)

        lifecycleScope.launch {
            historyDao.insert(HistoryEntity(date))
            Log.e("Create date: ", date)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }
}