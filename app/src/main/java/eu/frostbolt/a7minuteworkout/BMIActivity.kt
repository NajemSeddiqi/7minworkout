package eu.frostbolt.a7minuteworkout

import android.os.Bundle
import android.view.View
import android.widget.RadioGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import eu.frostbolt.a7minuteworkout.databinding.ActivityBmiactivityBinding
import java.math.BigDecimal
import java.math.RoundingMode

class BMIActivity : AppCompatActivity() {

    companion object {
        private const val METRIC_UNITS_VIEW = "METRIC_UNIT_VIEW"
        private const val US_UNITS_VIEW = "US_UNIT_VIEW"
    }

    private var binding: ActivityBmiactivityBinding? = null
    private var currentVisibleView: String = METRIC_UNITS_VIEW

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBmiactivityBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        setSupportActionBar(binding?.tbBmiActivity)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "CALCULATE BMI"
        binding?.tbBmiActivity?.setNavigationOnClickListener { onBackPressed() }

        showMetricUnitsView()
        binding?.rgUnits?.setOnCheckedChangeListener(::radioGroupOnCheckedChangeListener)

        binding?.btnCalculateBmi?.setOnClickListener(::calculateUnits)
    }

    private fun showMetricUnitsView() {
        binding?.rgUnits?.check(R.id.rbMetricUnits)

        currentVisibleView = METRIC_UNITS_VIEW

        binding?.tilMetricWeight?.visibility = View.VISIBLE
        binding?.tilMetricHeight?.visibility = View.VISIBLE

        binding?.tilUsUnitWeight?.visibility = View.GONE
        binding?.tilUsUnitHeightFeet?.visibility = View.GONE
        binding?.tilUsUnitHeightInches?.visibility = View.GONE

        binding?.llBmiResult?.visibility = View.INVISIBLE

        binding?.etUsUnitWeight?.text?.clear()
        binding?.etUsUnitHeightFeet?.text?.clear()
        binding?.etUsUnitHeightInches?.text?.clear()
    }

    private fun showUsUnitsView() {
        currentVisibleView = US_UNITS_VIEW

        binding?.tilUsUnitWeight?.visibility = View.VISIBLE
        binding?.tilUsUnitHeightFeet?.visibility = View.VISIBLE
        binding?.tilUsUnitHeightInches?.visibility = View.VISIBLE

        binding?.tilMetricWeight?.visibility = View.INVISIBLE
        binding?.tilMetricHeight?.visibility = View.INVISIBLE

        binding?.llBmiResult?.visibility = View.INVISIBLE

        binding?.etMetricWeight?.text?.clear()
        binding?.etMetricHeight?.text?.clear()
    }

    private fun radioGroupOnCheckedChangeListener(radioGroup: RadioGroup, id: Int) {
        if (id == R.id.rbMetricUnits) {
            showMetricUnitsView()
        } else {
            showUsUnitsView()
        }
    }

    private fun calculateMetricBmi() {
        if (metricUnitsAreValid()) {
            val height: Float = binding?.etMetricHeight?.text.toString().toFloat() / 100
            val weight: Float = binding?.etMetricWeight?.text.toString().toFloat()

            val bmi = weight / (height * height)
            displayBmiResult(bmi)
        } else {
            Toast.makeText(this, "Please enter valid values", Toast.LENGTH_SHORT).show()
        }
    }

    private fun calculateUsUnitBmi() {
        if(usUnitsAreValid()) {
            val heightFeetValue = binding?.etUsUnitHeightFeet?.text.toString()
            val heightInchesValue = binding?.etUsUnitHeightInches?.text.toString()
            val weightValue = binding?.etUsUnitWeight?.text.toString().toFloat()

            val heightValue = heightFeetValue.toFloat() + heightInchesValue.toFloat() * 12
            val bmi = 703 * (weightValue / (heightValue * heightValue))

            displayBmiResult(bmi)
        } else {
            Toast.makeText(this, "Please enter valid values", Toast.LENGTH_SHORT).show()
        }
    }

    private fun displayBmiResult(bmi: Float) {
        val (bmiType, bmiDescription) = bmiInformation(bmi)
        val bmiValue = BigDecimal(bmi.toDouble()).setScale(2, RoundingMode.HALF_EVEN).toString()

        binding?.llBmiResult?.visibility = View.VISIBLE

        binding?.tbBmiValue?.text = bmiValue
        binding?.tvBmiType?.text = bmiType
        binding?.tvBmiDescription?.text = bmiDescription

    }

    private fun bmiInformation(bmi: Float): Pair<String, String> {
        val bmiType: String
        val bmiDescription: String

        when {
            bmi.compareTo(15F) <= 0 -> {
                bmiType = "Very severely underweight"
                bmiDescription = "You really need to take better care of yourself! Eat more"
                return Pair(bmiType, bmiDescription)
            }
            bmi.compareTo(15F) > 0 && bmi.compareTo(16F) <= 0 -> {
                bmiType = "Severely underweight"
                bmiDescription = "You really need to take better care of yourself! Eat more"
                return Pair(bmiType, bmiDescription)
            }
            bmi.compareTo(16F) > 0 && bmi.compareTo(18.5F) <= 0 -> {
                bmiType = "Underweight"
                bmiDescription = "You really need to take better care of yourself! Eat more"
                return Pair(bmiType, bmiDescription)
            }
            bmi.compareTo(18.5F) > 0 && bmi.compareTo(25F) <= 0 -> {
                bmiType = "Normal"
                bmiDescription = "Congratulations! You are in good shape!"
                return Pair(bmiType, bmiDescription)
            }
            bmi.compareTo(25F) > 0 && bmi.compareTo(30F) <= 0 -> {
                bmiType = "Above normal"
                bmiDescription = "Either you're slightly overweight or you're built stocky! Do some cardio or keep building that muscle!"
                return Pair(bmiType, bmiDescription)
            }
            bmi.compareTo(30F) > 0 && bmi.compareTo(35F) <= 0 -> {
                bmiType = "Obese Class | (Moderately obese)"
                bmiDescription = "You really need to take better care of yourself! Workout more"
                return Pair(bmiType, bmiDescription)
            }
            bmi.compareTo(35F) > 0 && bmi.compareTo(40F) <= 0 -> {
                bmiType = "Obese Class || (Severely obese)"
                bmiDescription = "You are in a dangerous condition! Workout more"
                return Pair(bmiType, bmiDescription)
            }
            else -> {
                bmiType = "Obese Class ||| (Very Severely obese)"
                bmiDescription = "You are in a very dangerous condition! Workout more"
                return Pair(bmiType, bmiDescription)
            }
        }
    }

    private fun calculateUnits(it: View) {
        if(currentVisibleView == METRIC_UNITS_VIEW) {
            calculateMetricBmi()
        } else {
            calculateUsUnitBmi()
        }
    }

    private fun metricUnitsAreValid(): Boolean = binding?.etMetricWeight?.text.toString().isNotEmpty() && binding?.etMetricHeight?.text.toString().isNotEmpty()

    private fun usUnitsAreValid(): Boolean =
        when {
            binding?.etUsUnitWeight?.text.toString().isEmpty() -> false
            binding?.etUsUnitHeightFeet?.text.toString().isEmpty() -> false
            binding?.etUsUnitHeightInches?.text.toString().isEmpty() -> false
            else -> true
        }
}