package eu.frostbolt.a7minuteworkout.adapters

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import eu.frostbolt.a7minuteworkout.R
import eu.frostbolt.a7minuteworkout.databinding.ItemExerciseStatusBinding
import eu.frostbolt.a7minuteworkout.models.ExerciseModel

class ExerciseStatusAdapter(private val items: ArrayList<ExerciseModel>) : RecyclerView.Adapter<ExerciseStatusAdapter.ViewHolder>() {

    inner class ViewHolder(binding: ItemExerciseStatusBinding) : RecyclerView.ViewHolder(binding.root) {
        val tvItem = binding.tvItem
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemExerciseStatusBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model: ExerciseModel = items[position]
        holder.tvItem.text = (model.getId() + 1).toString()

        when {
            model.getIsSelected() -> {
                setCurrentExerciseStyle(holder, R.drawable.item_circular_thin_color_accent_border, "#212121")
            }
            model.getIsCompleted() -> {
                setCurrentExerciseStyle(holder, R.drawable.item_circular_color_accent_background, "#FFFFFF")
            }
            else -> {
                setCurrentExerciseStyle(holder, R.drawable.item_circular_color_gray_background, "#212121")
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    private fun setCurrentExerciseStyle(holder: ViewHolder, @DrawableRes id: Int, textColor: String) {
        holder.tvItem.background = ContextCompat.getDrawable(holder.itemView.context, id)
        holder.tvItem.setTextColor(Color.parseColor(textColor))
    }
}