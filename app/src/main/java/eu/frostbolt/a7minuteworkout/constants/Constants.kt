package eu.frostbolt.a7minuteworkout.constants

import eu.frostbolt.a7minuteworkout.R
import eu.frostbolt.a7minuteworkout.models.ExerciseModel

object Constants {
    fun defaultExercises(): ArrayList<ExerciseModel>{
        val exercises = ArrayList<ExerciseModel>()

        val abdominalCrunch = ExerciseModel(0, "Abdominal Crunch", R.drawable.ic_abdominal_crunch)
        val highKneesRunningInPlace = ExerciseModel(1, "High Knees Running", R.drawable.ic_high_knees_running_in_place)
        val jumpingJacks = ExerciseModel(2, "Jumping Jacks", R.drawable.ic_jumping_jacks)
        val lunge = ExerciseModel(3, "Lunge", R.drawable.ic_lunge)
        val plank = ExerciseModel(4, "Plank", R.drawable.ic_plank)
        val sidePlank = ExerciseModel(5, "Side Plank", R.drawable.ic_side_plank)
        val pushUp = ExerciseModel(6, "Push Up", R.drawable.ic_push_up)
        val pushUpRotation = ExerciseModel(7, "Push Up Rotation", R.drawable.ic_push_up_and_rotation)
        val squat = ExerciseModel(8, "Squat", R.drawable.ic_squat)
        val stepUp = ExerciseModel(9, "Step Up", R.drawable.ic_step_up_onto_chair)
        val tricepsDip = ExerciseModel(10, "Triceps Dip", R.drawable.ic_triceps_dip_on_chair)
        val wallSit = ExerciseModel(11, "Wall Sit", R.drawable.ic_wall_sit)

        exercises.add(abdominalCrunch)
        exercises.add(highKneesRunningInPlace)
        exercises.add(jumpingJacks)
        exercises.add(lunge)
        exercises.add(plank)
        exercises.add(sidePlank)
        exercises.add(pushUp)
        exercises.add(pushUpRotation)
        exercises.add(squat)
        exercises.add(stepUp)
        exercises.add(tricepsDip)
        exercises.add(wallSit)

        return exercises
    }
}